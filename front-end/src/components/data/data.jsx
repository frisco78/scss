import "./data.sass";

function Data({title, children}) {
    return (
        <p className="text">{title && <strong>{title} :</strong>} {children}</p>
    )
}

export default Data;