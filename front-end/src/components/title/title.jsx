import './title.sass'

function Title({children}) {
    return (
        <p className="title">
            {children}
        </p>
    )
}

export default Title;