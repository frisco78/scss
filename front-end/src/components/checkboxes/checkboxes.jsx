import { useEffect, useState } from "react"
import Checkbox from "./checkbox"

function Checkboxes({ titles = [], onCheck = () => { } }) {
    const [isChecked, setIsChecked] = useState({})

    useEffect((() => {
        let temp = {}
        titles.map((title) => {
            temp[title] = false
        })
        setIsChecked(temp)
        onCheck(temp)
    }), [titles])

    const handleCheck = (title) => {
        let temp = isChecked
        temp[title] = !temp[title]
        setIsChecked(temp)
        onCheck(isChecked)
    }

    return (
        <>
            {titles.map((title, key) => {
                return <Checkbox title={title} onCheck={handleCheck} key={key} />
            })}
        </>
    )
}

export default Checkboxes;
