import { useState } from "react";

function Checkbox({ title, onCheck = () => {} }) {
    const [isChecked, setIsChecked] = useState(false);

    const handleOnChange = () => {
        setIsChecked(!isChecked);
        onCheck(title)
    };

    return (
        <div>
            {title}<input className="input" type="checkbox" value={title} onChange={handleOnChange} checked={isChecked}/>
        </div>
    )
}

export default Checkbox;
