import "./input.sass"

function Input({ placeholder, onChange = () => { } }) {
    const handleChange = (event) => {
        onChange(event.target.value)
    }
    return (
        <textarea placeholder={placeholder} onChange={handleChange}></textarea>
    )
}

export default Input;