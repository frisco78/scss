import "./button.sass";

function Button({ onClick = () => { }, children }) {
    const handleClick = () => {
        onClick()
    }

    return (
        <form className={"button"} onSubmit={handleClick}>
            <input type="submit" value={children} />
        </form>
    )
}

export default Button
