import './greyBox.sass'

function GreyBox({children}) {
    return (
        <div className="greyBox">
            {children}
        </div>
    )
}

export default GreyBox;