import { Header } from "../../components/header/header";
import Button from "../../components/button/button";
import Data from "../../components/data/data";
import GreyBox from "../../components/greyBox/greyBox";
import Title from "../../components/title/title";
import { useState } from "react";
import Checkboxes from "../../components/checkboxes/checkboxes";
import Input from "../../components/input/input";

const vehicle = {
    "id": 1,
    "brand": "Pouette",
    "modele": "F",
    "seriesNumber": "727",
    "pieceToChange": "Claxon",
    "createdAt": "2023-10-23T12:56:47.000Z",
    "updatedAt": "2023-10-23T12:56:47.000Z"
}

const items = [
    "Rétroviseur(12EOOO)",
    "Pneu(13JE300)",
    "Allumage(25OOP0)",
    "Gallet(FER2002)",
    "Caburateur(458JUIED6)"
]

function InterventionVehicle() {
    const [isChecked, setIsChecked] = useState()
    const [comment, setComment] = useState("")
    const handleClick = () => {

    }

    const handleCheck = (isChecked) => {
        setIsChecked(isChecked);
    }

    const handleComment = (comment) => {
        setComment(comment);
    }

    return (
        <>
            <Header />
            <div className="container">
                <GreyBox>
                    <Title>Description du Scooter</Title>
                    <Data title="Marque">{vehicle.brand}</Data>
                    <Data title="Modèle">{vehicle.modele}</Data>
                    <Data title="Numéro de Série">{vehicle.seriesNumber}</Data>
                </GreyBox>

                <Title>Pièce remplacées</Title>
                <Checkboxes titles={items} onCheck={handleCheck}/>

                <Input placeholder={"Comment :"} onChange={handleComment}/>

                <Button onClick={handleClick}>Valider</Button>
            </div>
        </>
    )
}

export default InterventionVehicle;